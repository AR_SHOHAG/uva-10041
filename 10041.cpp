#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;

int main()
{
    int n, s, i, sum=0, mid;

    scanf("%d", &n);

    for(int j=1; j<=n;++j){
        scanf("%d", &s);
        int arr[s];
        for(i=0; i<s; ++i){
            scanf("%d", &arr[i]);
        }
        sort(arr, arr+s);
        mid = arr[s/2];
        for(i=0; i<s; ++i){
            sum += abs(mid-arr[i]);
        }

        printf("%d\n", sum);
        sum=0;

    }
    return 0;
}
